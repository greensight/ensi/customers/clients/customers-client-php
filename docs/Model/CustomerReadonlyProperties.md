# # CustomerReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор покупателя | 
**avatar** | [**\Ensi\CustomersClient\Dto\File**](File.md) |  | 
**new_email** | **string** | Новая почта | 
**email_token_created_at** | [**\DateTime**](\DateTime.md) | Дата создания токена эл. почты | 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания пользователя | 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления пользователя | 
**full_name** | **string** | Полное ФИО | 
**delete_request** | [**\DateTime**](\DateTime.md) | Дата и время запроса на удаление персональных данных | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


