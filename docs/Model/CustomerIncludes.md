# # CustomerIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addresses** | [**\Ensi\CustomersClient\Dto\CustomerAddress[]**](CustomerAddress.md) |  | [optional] 
**status** | [**\Ensi\CustomersClient\Dto\CustomerStatus**](CustomerStatus.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


