# # CustomerAddressFillablePropertiesAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_string** | **string** | Строка с полным описанием адреса | 
**post_index** | **string** | Почтовый индекс | [optional] 
**country_code** | **string** | Код страны | [optional] 
**region** | **string** | Регион | [optional] 
**region_guid** | **string** | Код ФИАС региона | [optional] 
**area** | **string** | Район | [optional] 
**area_guid** | **string** | Код ФИАС района | [optional] 
**city** | **string** | Город | [optional] 
**city_guid** | **string** | Код ФИАС города | 
**street** | **string** | Улица | [optional] 
**house** | **string** | Дом | [optional] 
**block** | **string** | Строение | [optional] 
**porch** | **string** | Подъезд | [optional] 
**intercom** | **string** | Домофон | [optional] 
**floor** | **string** | Этаж | [optional] 
**flat** | **string** | Квартира | [optional] 
**comment** | **string** | Комментарий к адресу | [optional] 
**geo_lat** | **float** | Ширина | [optional] 
**geo_lon** | **float** | Долгота | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


