# Ensi\CustomersClient\CustomersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCustomer**](CustomersApi.md#createCustomer) | **POST** /customers/customers | Создание объекта типа Customer
[**deleteCustomerAvatar**](CustomersApi.md#deleteCustomerAvatar) | **POST** /customers/customers/{id}:delete-avatar | Удаление аватара покупателя
[**deletePersonalData**](CustomersApi.md#deletePersonalData) | **POST** /customers/customers/{id}:delete-personal-data | Запрос на удаление персональных данных покупателя
[**getCustomer**](CustomersApi.md#getCustomer) | **GET** /customers/customers/{id} | Получение объекта типа Customer
[**patchCustomer**](CustomersApi.md#patchCustomer) | **PATCH** /customers/customers/{id} | Обновление объекта типа Customer
[**searchCustomer**](CustomersApi.md#searchCustomer) | **POST** /customers/customers:search-one | Поиск объекта типа Customer
[**searchCustomers**](CustomersApi.md#searchCustomers) | **POST** /customers/customers:search | Поиск объектов типа Customer
[**uploadCustomerAvatar**](CustomersApi.md#uploadCustomerAvatar) | **POST** /customers/customers/{id}:upload-avatar | Загрузка файла с аватаром покупателя
[**verifyEmailCustomer**](CustomersApi.md#verifyEmailCustomer) | **POST** /customers/customers:verify-email | Подтверждение изменения почты



## createCustomer

> \Ensi\CustomersClient\Dto\CustomerResponse createCustomer($create_customer_request)

Создание объекта типа Customer

Создание объекта типа Customer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_customer_request = new \Ensi\CustomersClient\Dto\CreateCustomerRequest(); // \Ensi\CustomersClient\Dto\CreateCustomerRequest | 

try {
    $result = $apiInstance->createCustomer($create_customer_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->createCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_customer_request** | [**\Ensi\CustomersClient\Dto\CreateCustomerRequest**](../Model/CreateCustomerRequest.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteCustomerAvatar

> \Ensi\CustomersClient\Dto\CustomerResponse deleteCustomerAvatar($id)

Удаление аватара покупателя

Удаление аватара покупателя из базы и файловой системы

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteCustomerAvatar($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->deleteCustomerAvatar: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CustomersClient\Dto\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deletePersonalData

> \Ensi\CustomersClient\Dto\EmptyDataResponse deletePersonalData($id)

Запрос на удаление персональных данных покупателя

Запрос на удаление персональных данных покупателя

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deletePersonalData($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->deletePersonalData: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CustomersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getCustomer

> \Ensi\CustomersClient\Dto\CustomerResponse getCustomer($id, $include)

Получение объекта типа Customer

Получение объекта типа Customer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getCustomer($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->getCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\CustomersClient\Dto\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchCustomer

> \Ensi\CustomersClient\Dto\CustomerResponse patchCustomer($id, $patch_customer_request)

Обновление объекта типа Customer

Обновление объекта типа Customer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_customer_request = new \Ensi\CustomersClient\Dto\PatchCustomerRequest(); // \Ensi\CustomersClient\Dto\PatchCustomerRequest | 

try {
    $result = $apiInstance->patchCustomer($id, $patch_customer_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->patchCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_customer_request** | [**\Ensi\CustomersClient\Dto\PatchCustomerRequest**](../Model/PatchCustomerRequest.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchCustomer

> \Ensi\CustomersClient\Dto\CustomerResponse searchCustomer($search_customers_request)

Поиск объекта типа Customer

Поиск объектов типа Customer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_customers_request = new \Ensi\CustomersClient\Dto\SearchCustomersRequest(); // \Ensi\CustomersClient\Dto\SearchCustomersRequest | 

try {
    $result = $apiInstance->searchCustomer($search_customers_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->searchCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_customers_request** | [**\Ensi\CustomersClient\Dto\SearchCustomersRequest**](../Model/SearchCustomersRequest.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchCustomers

> \Ensi\CustomersClient\Dto\SearchCustomersResponse searchCustomers($search_customers_request)

Поиск объектов типа Customer

Поиск объектов типа Customer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_customers_request = new \Ensi\CustomersClient\Dto\SearchCustomersRequest(); // \Ensi\CustomersClient\Dto\SearchCustomersRequest | 

try {
    $result = $apiInstance->searchCustomers($search_customers_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->searchCustomers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_customers_request** | [**\Ensi\CustomersClient\Dto\SearchCustomersRequest**](../Model/SearchCustomersRequest.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\SearchCustomersResponse**](../Model/SearchCustomersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## uploadCustomerAvatar

> \Ensi\CustomersClient\Dto\CustomerResponse uploadCustomerAvatar($id, $file)

Загрузка файла с аватаром покупателя

Загрузка файла с аватаром покупателя

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->uploadCustomerAvatar($id, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->uploadCustomerAvatar: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\CustomersClient\Dto\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## verifyEmailCustomer

> \Ensi\CustomersClient\Dto\CustomerResponse verifyEmailCustomer($verify_email_request)

Подтверждение изменения почты

Подтверждение изменения почты

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\CustomersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$verify_email_request = new \Ensi\CustomersClient\Dto\VerifyEmailRequest(); // \Ensi\CustomersClient\Dto\VerifyEmailRequest | 

try {
    $result = $apiInstance->verifyEmailCustomer($verify_email_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersApi->verifyEmailCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verify_email_request** | [**\Ensi\CustomersClient\Dto\VerifyEmailRequest**](../Model/VerifyEmailRequest.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\CustomerResponse**](../Model/CustomerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

