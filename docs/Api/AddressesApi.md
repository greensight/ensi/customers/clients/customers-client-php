# Ensi\CustomersClient\AddressesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCustomerAddress**](AddressesApi.md#createCustomerAddress) | **POST** /customers/addresses | Создание объекта типа CustomerAddress
[**deleteCustomerAddress**](AddressesApi.md#deleteCustomerAddress) | **DELETE** /customers/addresses/{id} | Удаление объекта типа CustomerAddress
[**getCustomerAddress**](AddressesApi.md#getCustomerAddress) | **GET** /customers/addresses/{id} | Получение объекта типа CustomerAddress
[**replaceCustomerAddress**](AddressesApi.md#replaceCustomerAddress) | **PUT** /customers/addresses/{id} | Замена объекта типа CustomerAddress
[**searchCustomerAddresses**](AddressesApi.md#searchCustomerAddresses) | **POST** /customers/addresses:search | Поиск объектов типа Customer Address
[**setCustomerAddressesAsDefault**](AddressesApi.md#setCustomerAddressesAsDefault) | **POST** /customers/addresses/{id}:set-as-default | Устанавливает Customer Address как адрес по-умолчанию



## createCustomerAddress

> \Ensi\CustomersClient\Dto\CustomerAddressResponse createCustomerAddress($create_customer_address_request)

Создание объекта типа CustomerAddress

Создание объекта типа CustomerAddress

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\AddressesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_customer_address_request = new \Ensi\CustomersClient\Dto\CreateCustomerAddressRequest(); // \Ensi\CustomersClient\Dto\CreateCustomerAddressRequest | 

try {
    $result = $apiInstance->createCustomerAddress($create_customer_address_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddressesApi->createCustomerAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_customer_address_request** | [**\Ensi\CustomersClient\Dto\CreateCustomerAddressRequest**](../Model/CreateCustomerAddressRequest.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\CustomerAddressResponse**](../Model/CustomerAddressResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteCustomerAddress

> \Ensi\CustomersClient\Dto\EmptyDataResponse deleteCustomerAddress($id)

Удаление объекта типа CustomerAddress

Удаление объекта типа CustomerAddress

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\AddressesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteCustomerAddress($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddressesApi->deleteCustomerAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CustomersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getCustomerAddress

> \Ensi\CustomersClient\Dto\CustomerAddressResponse getCustomerAddress($id, $include)

Получение объекта типа CustomerAddress

Получение объекта типа CustomerAddress

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\AddressesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getCustomerAddress($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddressesApi->getCustomerAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\CustomersClient\Dto\CustomerAddressResponse**](../Model/CustomerAddressResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceCustomerAddress

> \Ensi\CustomersClient\Dto\CustomerAddressResponse replaceCustomerAddress($id, $replace_customer_address_request)

Замена объекта типа CustomerAddress

Замена объекта типа CustomerAddress

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\AddressesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_customer_address_request = new \Ensi\CustomersClient\Dto\ReplaceCustomerAddressRequest(); // \Ensi\CustomersClient\Dto\ReplaceCustomerAddressRequest | 

try {
    $result = $apiInstance->replaceCustomerAddress($id, $replace_customer_address_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddressesApi->replaceCustomerAddress: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_customer_address_request** | [**\Ensi\CustomersClient\Dto\ReplaceCustomerAddressRequest**](../Model/ReplaceCustomerAddressRequest.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\CustomerAddressResponse**](../Model/CustomerAddressResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchCustomerAddresses

> \Ensi\CustomersClient\Dto\SearchCustomerAddressesResponse searchCustomerAddresses($search_customer_addresses_request)

Поиск объектов типа Customer Address

Поиск объектов типа Customer Address

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\AddressesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_customer_addresses_request = new \Ensi\CustomersClient\Dto\SearchCustomerAddressesRequest(); // \Ensi\CustomersClient\Dto\SearchCustomerAddressesRequest | 

try {
    $result = $apiInstance->searchCustomerAddresses($search_customer_addresses_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddressesApi->searchCustomerAddresses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_customer_addresses_request** | [**\Ensi\CustomersClient\Dto\SearchCustomerAddressesRequest**](../Model/SearchCustomerAddressesRequest.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\SearchCustomerAddressesResponse**](../Model/SearchCustomerAddressesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## setCustomerAddressesAsDefault

> \Ensi\CustomersClient\Dto\EmptyDataResponse setCustomerAddressesAsDefault($id)

Устанавливает Customer Address как адрес по-умолчанию

Устанавливает Customer Address как адрес по-умолчанию

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\AddressesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->setCustomerAddressesAsDefault($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddressesApi->setCustomerAddressesAsDefault: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CustomersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

