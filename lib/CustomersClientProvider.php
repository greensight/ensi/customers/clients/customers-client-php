<?php

namespace Ensi\CustomersClient;

class CustomersClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\CustomersClient\Api\AddressesApi',
        '\Ensi\CustomersClient\Api\StatusesApi',
        '\Ensi\CustomersClient\Api\CustomersApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\CustomersClient\Dto\MultipartFileUploadRequest',
        '\Ensi\CustomersClient\Dto\RequestBodyPagination',
        '\Ensi\CustomersClient\Dto\SearchCustomersResponse',
        '\Ensi\CustomersClient\Dto\Customer',
        '\Ensi\CustomersClient\Dto\Error',
        '\Ensi\CustomersClient\Dto\SearchCustomerStatusesRequest',
        '\Ensi\CustomersClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\CustomersClient\Dto\PaginationTypeEnum',
        '\Ensi\CustomersClient\Dto\SearchCustomerStatusesResponse',
        '\Ensi\CustomersClient\Dto\CustomerAddress',
        '\Ensi\CustomersClient\Dto\File',
        '\Ensi\CustomersClient\Dto\CustomerAddressFillablePropertiesAddress',
        '\Ensi\CustomersClient\Dto\EmptyDataResponse',
        '\Ensi\CustomersClient\Dto\CustomerAddressFillableProperties',
        '\Ensi\CustomersClient\Dto\CreateCustomerStatusRequest',
        '\Ensi\CustomersClient\Dto\CustomerEmail',
        '\Ensi\CustomersClient\Dto\PatchCustomerStatusRequest',
        '\Ensi\CustomersClient\Dto\ResponseBodyPagination',
        '\Ensi\CustomersClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\CustomersClient\Dto\CustomerAddressResponse',
        '\Ensi\CustomersClient\Dto\CustomerStatusFillableProperties',
        '\Ensi\CustomersClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\CustomersClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\CustomersClient\Dto\CustomerStatus',
        '\Ensi\CustomersClient\Dto\CustomerReadonlyProperties',
        '\Ensi\CustomersClient\Dto\ErrorResponse',
        '\Ensi\CustomersClient\Dto\CustomerStatusReadonlyProperties',
        '\Ensi\CustomersClient\Dto\ReplaceCustomerAddressRequest',
        '\Ensi\CustomersClient\Dto\CustomerNullableEmail',
        '\Ensi\CustomersClient\Dto\SearchCustomerAddressesResponse',
        '\Ensi\CustomersClient\Dto\MessageForChangeEmail',
        '\Ensi\CustomersClient\Dto\SearchCustomersRequest',
        '\Ensi\CustomersClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\CustomersClient\Dto\SearchCustomerAddressesRequest',
        '\Ensi\CustomersClient\Dto\RequestBodyCursorPagination',
        '\Ensi\CustomersClient\Dto\ModelInterface',
        '\Ensi\CustomersClient\Dto\CreateCustomerRequest',
        '\Ensi\CustomersClient\Dto\VerifyEmailRequest',
        '\Ensi\CustomersClient\Dto\CustomerResponse',
        '\Ensi\CustomersClient\Dto\CustomerIncludes',
        '\Ensi\CustomersClient\Dto\CreateCustomerAddressRequest',
        '\Ensi\CustomersClient\Dto\CustomerGenderEnum',
        '\Ensi\CustomersClient\Dto\SearchCustomersResponseMeta',
        '\Ensi\CustomersClient\Dto\PatchCustomerRequest',
        '\Ensi\CustomersClient\Dto\CustomerStatusResponse',
        '\Ensi\CustomersClient\Dto\ErrorResponse2',
        '\Ensi\CustomersClient\Dto\CustomerAddressReadonlyProperties',
        '\Ensi\CustomersClient\Dto\CustomerFillableProperties',
    ];

    /** @var string */
    public static $configuration = '\Ensi\CustomersClient\Configuration';
}
